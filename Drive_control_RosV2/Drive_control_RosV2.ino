
#include <ros.h>
#include <perris_first_steps/IntList.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>
#include <std_msgs/Int16.h>


//I am a terrible programmer so I am using global varibles, I do declare.

bool inFailSafe=true;

int a=0;
int b=0;
int c=0;
int d=0;
int e=0;
int f=0;

int cmillis=millis();

int lapse=1500;
unsigned long stoptime=0+lapse;

ros::NodeHandle ArdMegDrive;
std_msgs::String str_msg;
ros::Publisher log_info("rover_log", &str_msg);



void rosmsg( const perris_first_steps::IntList& msg){
  inFailSafe=false;
  a=msg.data[0];
  b=msg.data[1];
  //c=msg.data[2];
  //d=msg.data[3];
  //e=msg.data[4];
  //f=msg.data[5];
  stoptime=cmillis+lapse;
  sendSerial();
}
void getstat(const std_msgs::Empty& getstat){
    str_msg.data="ArdMegDrive:Running";
    log_info.publish(&str_msg);
  
    ArdMegDrive.spinOnce();  
}


ros::Subscriber<perris_first_steps::IntList> sub("chatter", rosmsg );


ros::Subscriber<std_msgs::Empty> sublog("get_rover_log", getstat);

void setup() {
  //open the serial connection via ros
  //the following command must be run on the host to open the connection on that end
  //rosrun rosserial_arduino serial_node.py /dev/ttyACM0 
  //ACM0 may need to change depending on the mounting order
  ArdMegDrive.initNode();
  //subscribe to the node
  ArdMegDrive.subscribe(sub);
  ArdMegDrive.subscribe(sublog);
  ArdMegDrive.advertise(log_info);

  //attach each servo to respective pin
  str_msg.data="Drive arduino intilized";
  delay(5000);
  log_info.publish(&str_msg);
  Serial1.begin(115200);

}
void failSafe(){
  //these are the fail safe values to change what stop mode is change these values
  stoptime=cmillis+lapse;
  a=0;
  b=0;
  c=0;
  d=0;
  e=0;
  f=0;
  sendSerial();
}

void sendSerial(){
Serial1.print("<"+String(a)+","+String(b)+","+String(c)+","+String(d)+","+String(e)+","+String(f)+">");  

}

void loop() {
  ArdMegDrive.spinOnce();
  cmillis=millis();
  //if (cmillis>stoptime){
  //  failSafe();
  //}
  if (Serial1.available()>0){
    ;//this is if we want to read data back
  }


  
  delay(100);
}
