#ifndef _ROS_claw_control_Int16List_h
#define _ROS_claw_control_Int16List_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace claw_control
{

  class Int16List : public ros::Msg
  {
    public:
      int16_t data[6];

    Int16List():
      data()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      for( uint32_t i = 0; i < 6; i++){
      union {
        int16_t real;
        uint16_t base;
      } u_datai;
      u_datai.real = this->data[i];
      *(outbuffer + offset + 0) = (u_datai.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_datai.base >> (8 * 1)) & 0xFF;
      offset += sizeof(this->data[i]);
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      for( uint32_t i = 0; i < 6; i++){
      union {
        int16_t real;
        uint16_t base;
      } u_datai;
      u_datai.base = 0;
      u_datai.base |= ((uint16_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_datai.base |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->data[i] = u_datai.real;
      offset += sizeof(this->data[i]);
      }
     return offset;
    }

    const char * getType(){ return "claw_control/Int16List"; };
    const char * getMD5(){ return "8a3ea6140910d4962e44fd92d92608fb"; };

  };

}
#endif
