//this program is for moving the arm to a state given by the a ROS node named claw_topic
//the format is as follows:
//<th,r,z,sig,alpha,gamma>
// < indicates the begining of a messege
// , is used as a delimeter
// > indicates the end of a messege
// th indicates the angle in degrees of the base
// r represents the horizontal distance between the base axis of rotation and tip of the claw
// z represents the vertical distance from the bottom of the base to the tip of the claw
// sig represents the angle (relitve to horizontal) that the claw should face.
// alpha indicates the rotation of the wrist of the arm in degrees NEED REFERENCE POINT
// gamma indicates the closed or open state of the claw in degrees NEED REFERENCE POINT
//
//pinouts are as follows
//With the plugs layed flat, with the metal side up the wire of the left is singal and the one on the right is ground
//All ground wires should be attatched to the ground of the arduino
//Base servo (DOF 0) signal is connected to pin 3, associated values are marked with an a
//Shoulder servo (DOF 1) signal is connected to pin 5, associated values are marked with an b
//Elbow servo (DOF 2) signal is connected to pin 6, associated values are marked with an c
//Wrist servo (DOF 3) signal is connected to pin 9, associated values are marked with an d
//Wrist rotation servo (DOF 4) signal is connected to pin 10, associated values are marked with an e
//Claw servo (DOF 5) signal is connected to pin 11, associated values are marked with an f
//
//the voltage for the arm should be provided from a source besides the arduino
//


//the servo library is used at pin 5 and 6 will use 980hz and be incompadable otherwise
#include <Servo.h>
#include <ros.h>
#include <arm/int16List.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>

//make servo objects for writting
//pins and axies are assigned in the setup loop
Servo servoa;
Servo servob;
Servo servoc;
Servo servod;
Servo servoe;
Servo servof;




//I am a terrible programmer so I am using global varibles, I do declare.


//This bunch is for read in values, they are assigned from the serial input
//The default values are to set the arm in a newtral position.
float th=0;
float r=114;
float z=58;
float alpha=8;
float gamma=0;
float sig=-8;

//these are the legnths of the segments of the arm, currently they are in 1/10 of inch,
//these are the only values that need to be changed for units to be changed
const float L1=176.3;
const float L2=161.2;
const float L3=27.5;

//here are some offsets to put the origin at the elbow pivot
/*these are unused now
float r_offset=62.5;
float z_offset=7.5;
float cam_offset=2.5;
*/

//This is pi because I did not want to import a library to use it
const float pi=3.14159265358979323846264338327950;

//indicates the max angle physical angle of each joint
const float a_max=90;
const float b_max=103;
const float c_max=145;
const float d_max=229;
const float e_max=0;
const float f_max=100;

//indicates the min angle physical angle of each joint
const float a_min=-90;
const float b_min=-4.5;
const float c_min=35;
const float d_min=143;
const float e_min=136;
const float f_min=0;

//indicates the min pulse length for each servo
const int apmin=1225;
const int bpmin=1140;
const int cpmin=1100;
const int dpmin=1500;
const int epmin=1275;
const int fpmin=1225;

//indicates the max pulse legnth for each servo
const int apmax=1750;//techincally 2000, but we only want 90° in each direction 1487 is zero
const int bpmax=2000;
const int cpmax=1725;
const int dpmax=1950;
const int epmax=2000;
const int fpmax=1750;

//These values are the in degrees they need to be converted to pulse length
//they should be reassigned before being written the first time
float a=0;
float b=0;
float c=0;
float d=0;
float e=0;
float f=0;

//These values are in pulse length and will be written to the servos
//they shoulld be reassigned before being written the first time
int ap=0;
int bp=0;
int cp=0;
int dp=0;
int ep=0;
int fp=0;

ros::NodeHandle clawArd;
std_msgs::String str_msg;
ros::Publisher log_info("rover_log", &str_msg);

void rosmsg( const arm::int16List& msg){
  th=msg.data[0];
  r=msg.data[1];
  z=msg.data[2];
  sig=msg.data[3];
  alpha=msg.data[4];
  gamma=msg.data[5];

}
void getstat(const std_msgs::Empty& getstat){
    str_msg.data="ClawArd:Running";
    log_info.publish(&str_msg);
    clawArd.spinOnce();  
}


ros::Subscriber<arm::int16List> sub("cArm", rosmsg );


ros::Subscriber<std_msgs::Empty> sublog("get_rover_log", getstat);

//values for reading the serial bus
const byte buffSize = 40;             //max charactor length of the incoming message
           //array to store the incoming message
const char startMarker = '<';         //indicates the start charactor of the message
const char endMarker = '>';           //indicates the end charactor of the message
byte bytesRecvd = 0;                  //counts the length of the message to avoid overflow
boolean readInProgress = false;       //indicates if a message is being read from the PC

void setup() {
  //open the serial connection via ros
  //the following command must be run on the host to open the connection on that end
  //rosrun rosserial_arduino serial_node.py /dev/ttyACM0 
  //ACM0 may need to change depending on the mounting order
  clawArd.initNode();
  //subscribe to the node
  clawArd.subscribe(sub);
  clawArd.subscribe(sublog);
  clawArd.advertise(log_info);
  //attach each servo to respective pin
  servoa.attach(3);
  servob.attach(5);
  servoc.attach(6);
  servod.attach(9);
  servoe.attach(10);
  servof.attach(11);
  str_msg.data="Claw arduino intilized";
  delay(5000);
  log_info.publish(&str_msg);
    
}

void calcAngles(){
  //this function will take the values of th,r,z,sig,alpha,and gamma
  //and returns a,b,c,d,e,f in degrees.
 
  //a,e,f are assigned directly
  f=gamma;
  e=alpha;
  a=th;
 
  //b,c,d are more complex
  //all 3 angles are confined to the plane
  //the wrist joint is found at (x2,y2), with r2 being the distance to the shoulder joint, and beta being the angle of r2
  float x2 = r - L3*cos(sig*pi/180);
  float y2 = z - L3*sin(sig*pi/180);
  float r2= sqrt(x2*x2+y2*y2);
  float beta = atan2(y2,x2);//in radians
 
  //the law of cosines gives angle c in radians, which is then converted to degrees
  c=(acos((L1*L1+L2*L2-r2*r2)/(2*L1*L2)))*180/pi;
 
  //the law of cosines gives the angle between shoulder beam and r2
  //adding this to beta gives b in radians, which can then be converted to degrees  
  b=(beta +acos((L1*L1-L2*L2+r2*r2)/(2*L1*r2)))*180/pi;
 
  //because b,c,d are confied to a plane they must add up to the target angle sigma (already in degrees)
  d=-sig+b+c;
}


void mapAngles(){
  //map the angles to the corasponding pulse lengths
  //do not edit equations here, edit the constants above
  ap=(a-a_min)/(a_max-a_min)*(apmax-apmin)+apmin;
  bp=(b-b_min)/(b_max-b_min)*(bpmax-bpmin)+bpmin;
  cp=(c-c_min)/(c_max-c_min)*(cpmax-cpmin)+cpmin;
  dp=(d-d_min)/(d_max-d_min)*(dpmax-dpmin)+dpmin;
  ep=(e-e_min)/(e_max-e_min)*(epmax-epmin)+epmin;
  fp= (f - f_min) / (f_max - f_min) * (fpmax - fpmin) + fpmin;
}

void updatePos(){
  //write the pulese lengths to the servos
  servoa.writeMicroseconds(ap);
  servob.writeMicroseconds(bp);
  servoc.writeMicroseconds(cp);
  servod.writeMicroseconds(dp);
  servoe.writeMicroseconds(ep);
  servof.writeMicroseconds(fp);
}



void loop() {
  clawArd.spinOnce();
  //char buf[8];
  //itoa(test,buf,10);
  //str_msg.data=buf;
  //test++;
  calcAngles();
  mapAngles();
  updatePos();
  delay(100);
}
